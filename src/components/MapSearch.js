import React, { Component } from 'react';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'

class MapSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            location: undefined
        };
        this.onChange = (location) => this.setState({ location })
    }

    handleSearch = (event) => {
        event.preventDefault();

        geocodeByAddress(this.state.location)
            .then(results => getLatLng(results[0]))
            .then(latLng => {
                console.log('Success', latLng);
                this.props.result(latLng);
            })
            .catch(error => console.error('Error', error))
    };

    render() {
        const inputProps = {
            value: this.state.location,
            onChange: this.onChange,
        };

        const options = {
            location: new google.maps.LatLng(51, 0),
            radius: 2000,
            types: ['geocode']
        };

        return (
            <div className="mapSearch">
                <form className="mapSearch__form" onSubmit={this.handleSearch}>
                    <PlacesAutocomplete inputProps={inputProps} options={options}/>
                    <button type="submit">Search</button>
                </form>
            </div>
        )
    }
}
export default MapSearch;