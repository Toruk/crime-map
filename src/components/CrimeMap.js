import React, { Component } from 'react'
import GMap from 'google-map-react';

import Marker from './Marker';

class CrimeMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cords: {lat: 51.5074, lng: 0.1278},
            zoom: 15,
            crimes: []
        }
    }

    componentDidMount() {
        this.setState({
            crimes: this.props.crimes
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.cords !== this.state.cords || nextProps.crimes !== this.state.crimes) {
            this.setState({ cords: nextProps.cords, crimes: nextProps.crimes });
        }
    }

    render() {
        return (
            <div className="crimeMap">
                <GMap
                    bootstrapURLKeys={{key: "AIzaSyDpRBEEDrPavlZHcxyL6cxJdoDqZDQPf5k"}}
                    center={this.state.cords}
                    zoom={this.state.zoom}>
                    {this.state.crimes.map((crime, key) => <Marker data={crime} lat={crime.location.latitude} lng={crime.location.longitude} key={key} />)}
                </GMap>
            </div>
        )
    }
}
export default CrimeMap