import React, { Component } from 'react'

class Marker extends Component {
    render() {
        return (
            <div className="marker" key={this.props.key}>
                <div className='marker__pin'></div>
                <div className='marker__pulse'></div>
            </div>
        )
    }
}
export default Marker;