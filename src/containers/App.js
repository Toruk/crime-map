import React, { Component } from 'react'
import Axios from 'axios';
import CrimeMap from "../components/CrimeMap";
import MapSearch from "../components/MapSearch";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            crimeData: null,
            cords: {lat: 51.5074, lng: 0.1278},
            loading: false
        }
    }

    componentDidMount() {
        // request crime data from the police.uk api
        this.fetchPoliceData();
    }

    fetchPoliceData() {
        // Change loading state when loading data
        this.setState({loading: true});
        Axios({url: `https://data.police.uk/api/crimes-street/all-crime?lat=${this.state.cords.lat}&lng=${this.state.cords.lng}&date=2017-08`, method: 'GET', timeout: 10000})
            .then(res => res.data)
            .then(json => {
                this.setState({
                    crimeData: json,
                    loading: false
                });
            })
            .catch(err => {
                this.setState({ loading: false });
                console.log('error - request timed out', err)
            })
    };

    handleSearchResult(data) {
        this.setState({
            cords: {lat: data.lat, lng: data.lng}
        });
        this.fetchPoliceData();
    }

    renderMap() {
        if (this.state.crimeData) {
            return <CrimeMap crimes={this.state.crimeData} cords={this.state.cords} />
        }
    }

    renderLoading() {
        if (this.state.loading) {
            return <div className="loading"><i className="loading__indicator"></i></div>
        }
    }

    render() {
        return (
            <div className="app">
                {/* Search */}
                <MapSearch result={this.handleSearchResult.bind(this)}/>
                {/* Map */}
                {this.renderMap()}
                {this.renderLoading()}
            </div>
        )
    }
}
export default App


