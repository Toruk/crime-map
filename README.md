# Crime Map

### Setup

```
> npm install
> npm start
```


### Notes

- Try searching for small towns as large areas eg london has performance issues. I would Ideally implement some sort of clustering. However I didn't get enough time to implement that feature.

- Would have also liked to implement a tooltip so you could tell what each crime is. However I ran out of time.