const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const HOST = 8000;

module.exports = [
    {
        entry: ['webpack-dev-server/client?http://localhost:' + HOST,'./src/index.js', './src/scss/main.scss'],
        output: {
            path: path.resolve(__dirname, 'public'),
            publicPath: "/",
            filename: "bundle.js"
        },
        module: {
            loaders: [
                { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
                { // sass / scss loader for webpack
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract('css-loader!sass-loader')
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192
                            }
                        }
                    ]
                }
            ]
        },
        devServer: {
            historyApiFallback: true,
            noInfo: true,
            port: HOST,
            headers: { 'Access-Control-Allow-Origin': '*' }
        },
        plugins: [
            new ExtractTextPlugin("styles.css")
        ]
    }
];